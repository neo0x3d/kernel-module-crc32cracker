# if KERNELRELEASE is defined, we've been invoked from the
# kernel build system and can use its language.
#ifneq (${KERNELRELEASE},)
#obj-m := crc32cracker.o
# Otherwise we were called directly from the command line.
# Invoke the kernel build system.
#else
#    KERNEL_SOURCE := ~/linux
#    PWD := $(shell pwd)
#default:
#	${MAKE} -C ${KERNEL_SOURCE} SUBDIRS=${PWD} modules

#clean:
#	${MAKE} -C ${KERNEL_SOURCE} SUBDIRS=${PWD} clean
#endif



obj-m += crc32cracker.o
KERNEL_SOURCE := ~/linux

# specify the built output dir (has to be present) and out of tree module dir
all:
	mkdir -p /lib/modules/$(shell uname -r)/build/
	make -C ${KERNEL_SOURCE} M=$(PWD) modules
	#make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) modules

clean:
	make -C ${KERNEL_SOURCE} M=$(PWD) clean
	#make -C /lib/modules/$(shell uname -r)/build/ M=$(PWD) clean

remove: #exec as root
	rm -f /dev/CRC32_HASHCRACK
	rmmod crc32cracker

install: #exec as root
	insmod crc32cracker.ko
	mknod /dev/CRC32_HASHCRACK c 243 0

test: #exec as root
	echo "deadbeef" > /dev/CRC32_HASHCRACK
	cat /dev/CRC32_HASHCRACK
	
addtokernel: #decide if crc32crack or crc32cracker
	mkdir -p ${KERNEL_SOURCE}/drivers/crc32cracker
	cp crc32cracker.c ${KERNEL_SOURCE}/drivers/crc32cracker/
	cp Makefile_kernel ${KERNEL_SOURCE}/drivers/crc32cracker/Makefile
	cp Kconfig ${KERNEL_SOURCE}/drivers/crc32cracker/
	# manually add; source "drivers/crc32cracker/Kconfig"
	# to ~/linux/Kconfig
	# and manually add; CONFIG_CRC32_HASHCRACK=y
	# to ~/linux/arch/arm64/configs/defconfig (defconfig for this specific raspberry)
	
	# with menuconfig it can be access on the top level as cr32cracker
