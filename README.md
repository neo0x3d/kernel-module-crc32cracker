# Linux Kernel module to brute force crack a crc32 hash as character device

Written for the Raspberry Pi 3. Should be compatible with other platforms (modification of Makefile required)

# Build, load and remove


 ```
 make
 # insmod crc32cracker.ko
 ```

 Check kernel messages with "dmesg", this should show that the module has been loaded and under which mayor/minor number it registered. Create the /dev/ entry with this mayor/minor node number, e.g. (mayor 243, minor 0)

 ```
 # mknod /dev/CRC32_HASHCRACKER c 243 0
 ```

Unload the kernel module and remove the /dev/ entry

 ```
 # rmmod /dev/CRC32_HASHCRACKER
 # rm /dev/CRC32_HASHCRACKER
 ```

# Usage

Write crc32 to be cracked to device node:
 ```
 # echo "0xdeadbeef" > /dev/CRC32_HASHCRACKER # change hash?
 # cat /dev/CRC32_HASHCRACKER
 ```

# TODO
 * DONE - Implementatin of a basic module including read/write intefaces
 * DONE - Implementation of the poll interface
 * DONE - Implementation of a queuing mechanism to allow for more than one crack operation to be delivered to the module
 * DONE - Add module to linux configuration subsystem to allow selection during configuration

# WONTFIX
 * Cannot remove kernel module via rmmod while a task is running

