/*
    This module implements a crc32 cracker kernel module

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <linux/types.h>
#include <linux/init.h>
#include <linux/fs.h> /*Needed by char devices*/
#include <linux/module.h>
#include <linux/interrupt.h>
#include <linux/cdev.h>/*Char device structures and functions*/
#include <linux/kdev_t.h>	/* needed for the dynamic major and minor number creation*/
#include <asm/uaccess.h> /*Interaction with user space*/
#include <linux/poll.h> /*support for poll, epoll and selct function calls; provides wait queue data structures and functions*/
#include <generated/utsrelease.h> /*defines UTS_RELEASE macro - Kernel version number*/
#include <linux/sched.h>
#include <linux/kthread.h>
#include <linux/workqueue.h>
#include <linux/slab.h>
#include <linux/list.h>

/******************************************************************************
 * Typedefs and defines
 *****************************************************************************/

#define UINT32_MAX ((uint32_t)(0xFFFFFFFF))

#define DEVICE_NAME "CRC32_HASHCRACK" /*Identifier used by the interrupt service routine*/

/* crc32 table, required for crc32() */
static u32 crc32_tab[] = {
    0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
    0xe963a535, 0x9e6495a3, 0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
    0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
    0xf3b97148, 0x84be41de, 0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
    0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec, 0x14015c4f, 0x63066cd9,
    0xfa0f3d63, 0x8d080df5, 0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
    0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
    0xdbbbc9d6, 0xacbcf940, 0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
    0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
    0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
    0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
    0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
    0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
    0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
    0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
    0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
    0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
    0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
    0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
    0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
    0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
    0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
    0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
    0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
    0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
    0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
    0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
    0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
    0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
    0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
    0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
    0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
    0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
    0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
    0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21, 0x86d3d2d4, 0xf1d4e242,
    0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
    0x88085ae6, 0xff0f6a70, 0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
    0x616bffd3, 0x166ccf45, 0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
    0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db, 0xaed16a4a, 0xd9d65adc,
    0x40df0b66, 0x37d83bf0, 0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
    0xbdbdf21c, 0xcabac28a, 0x53b39330, 0x24b4a3a6, 0xbad03605, 0xcdd70693,
    0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
    0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};


/******************************************************************************
 * Driver specific function prototypes
 *****************************************************************************/

u32 crc32(const void *buf, size_t size); // make static?

static int crc32hashcrack_open(struct inode* inode,
                               struct file* file);

static int crc32hashcrack_close(struct inode* inode,
                                struct file* file);

ssize_t crc32hashcrack_read(struct file *p_file,
                            char __user *p_buf,
                            size_t count,
                            loff_t *p_pos);

ssize_t crc32hashcrack_write(struct file *p_file,
                             const char __user *p_buf,
                             size_t count,
                             loff_t *p_pos);

static unsigned int crc32hashcrack_poll (struct file *file, poll_table *wait);



/******************************************************************************
 * Local variables and function prototypes
 *****************************************************************************/

/*
structure to keep everything in one place
*/
struct crc32hashcrack {

    /* dev_t is a 32bit variable with 12 bits for the major number and 20 for the minor 	number
       use MAJOR(dev_t dev); and
       MINOR(dev_t dev); to get the major and minor number
       see http://www.makelinux.net/ldd3 chapter 3.2.2.
    */
    dev_t devno;

    /* read queue */
    wait_queue_head_t read_q;

    /* character device type administrative data structure */
    struct cdev cdev;

    /* used to keep track if the device is used by a userspace tool already */
    bool in_use;
};

static struct crc32hashcrack crc32hashcrack_dev;

/*
linked list in kernel from: http://www.roman10.net/2011/07/28/linux-kernel-programminglinked-list/
*/
struct cracked_results_list {
    struct list_head list; // kernels list structure
    u32 input;
    u32 result;
};

static struct cracked_results_list cracked_list_head;

/*
workqueue from: https://jlmedina123.wordpress.com/2016/05/18/workqueue/ , or even better; https://notes.shichao.io/lkd
*/
static struct workqueue_struct *my_workqueue;

struct work_data {
    struct work_struct work;
    u32 data;
};

/*
Attach driver specific functions to Linux Kernel file operation structure.
These functions are used by the Kernel to access the device driver.

For further Information see http://www.makelinux.net/ldd3/chp-3-sect-3.shtml
*/
static struct file_operations crc32hashcrack_fops = {
    .owner   = THIS_MODULE,
    .read    = crc32hashcrack_read,
    .write   = crc32hashcrack_write,
    .open    = crc32hashcrack_open,
    .release = crc32hashcrack_close,
    .poll	   = crc32hashcrack_poll,
    //.unlocked_ioctl = crc32hashcrack_ioctl,
};


/******************************************************************************
 * Local functions
 *****************************************************************************/

u32 crc32(const void *buf, size_t size)
{
    const u8 *p = buf;
    u32 crc = UINT32_MAX;

    while (size-- != 0)
        crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);

    return crc ^ UINT32_MAX;
}

int crc32hashcrack_setup(struct crc32hashcrack *dev)
{
    struct cdev *cdev = &(dev->cdev);
    cdev_init(cdev, &crc32hashcrack_fops);
    cdev->owner = THIS_MODULE;
    cdev->ops = &crc32hashcrack_fops;
    init_waitqueue_head(&dev->read_q);
    return 0;
}


/*
worker function, runs int he work queue
*/
static void work_func(struct work_struct *work)
{
    struct work_data * data = (struct work_data *)work;
    printk("worker executing: 0x%08x \n", data->data);

    u32 i;
    i = 0;
    char buffer[30];
    u32 cur_crc;

    while (1) {
        /* arr, probably don't need to call cond_resched() all the time, every 1000 */
        cond_resched();
        u8 in[] = { (i >> 24), (i >> 16), (i >> 8), i };

        cur_crc = crc32(in, sizeof(in));

        if (cur_crc == data->data)
            break;
        i++;
    }

    snprintf(buffer, sizeof(buffer), "cracked is: 0x%08x\n", i);
    printk("%s\n", buffer);

    // when cracked add result to the list to be picked up by read()
    struct cracked_results_list *newitem;
    newitem = kmalloc(sizeof(*newitem), GFP_KERNEL);
    if (NULL == newitem)
    {
        printk(KERN_ALERT"Allocating memory failed! :(\n");
    }
    else
    {
        newitem->input=data->data;
        newitem->result=i;
        INIT_LIST_HEAD((struct list_head *)newitem);
        list_add_tail(&(newitem->list), &(cracked_list_head.list));
    }
    kfree((void *)data);
    return;

}

/******************************************************************************
 * Device functions
 *****************************************************************************/

/*
.open, allows only for a single user
*/
static int crc32hashcrack_open(struct inode* inode,
                               struct file* file)
{
    if(!crc32hashcrack_dev.in_use)
    {
        crc32hashcrack_dev.in_use = true;
        return 0;
    }
    else
    {
        return -EBUSY;
    }
}

/*
.close, allows only for a single user
*/
static int crc32hashcrack_close(struct inode* inode,
                                struct file* file)
{
    if(crc32hashcrack_dev.in_use)
    {
        crc32hashcrack_dev.in_use = false;
        return 0;
    }
    else
    {
        return -EIO;
    }
}

/*
.read function to return cracked results to the user if any are available
*/
ssize_t crc32hashcrack_read(struct file *p_file,
                            char __user *p_buf,
                            size_t count,
                            loff_t *p_pos)
{
    char buffer[70];
    int ret;

    if(list_empty(&cracked_list_head.list))
    {
        printk("Result list empty, cannot provide data on read()\n");
        return 0;
    }
    else
    {
        struct cracked_results_list *pulled;
        pulled = list_first_entry_or_null(&cracked_list_head.list, struct cracked_results_list,list);
        if (NULL == pulled)
        {
            printk(KERN_ALERT"Race condition or what? WTF?\n");
            return -1;
        }
        ret = snprintf(buffer, sizeof(buffer), "writeback to user: cracked!, input: 0x%08X, output: 0x%08X\n", pulled->input, pulled->result);
        list_del_init(&pulled->list); /* remove the item and re-init the head */
        kfree(pulled);
        if (0 == ret)
        {
            printk("Error on snprintf :(\n");
        }
        else
        {
            if(0 != copy_to_user(p_buf, buffer, ret))
            {
                printk(KERN_ALERT"Failed on copy_to_user! :(\n");
                return -1;
            }
        }
        return ret;
    }
}

/*
support non blocking IO including poll() and select()
*/
static unsigned int crc32hashcrack_poll(struct file *file, poll_table *wait)
{
    poll_wait(file, &(crc32hashcrack_dev.read_q), wait);
    if (!list_empty(&cracked_list_head.list))
        return POLLIN | POLLRDNORM;
    return 0;
}

/*
.write function to receive jobs from the user
*/
ssize_t crc32hashcrack_write(struct file *p_file,
                             const char __user *p_buf,
                             size_t count,
                             loff_t *p_pos)
{
    char	buffer[256];
    u32	crc32;
    printk(KERN_ALERT"Attempting to crack 0x%08X \n", crc32);

    if(0 != copy_from_user(buffer, p_buf, count))
    {
        printk(KERN_ALERT"Error at copy_from_user! :(\n");
        return -1;
    }
    sscanf(buffer, "%x", &crc32);
    printk(KERN_ALERT"Attempting to crack 0x%08X \n", crc32);


    struct work_data * data;
    data  = (struct work_data *)kmalloc(sizeof(struct work_data), GFP_KERNEL);
    if (NULL == data)
    {
        printk(KERN_ALERT"Allocating memory failed! :(\n");
    }
    else
    {
    		INIT_WORK((struct work_struct *)data, work_func);
    		data->data=crc32;
    		queue_work(my_workqueue, (struct work_struct *)data);
    		// alternatively to queue_work() (=own, custom thread) a system work queue called "events" can be used
    		//schedule_work(&data->work);
		}
    return count;
}


static int __init crc32hashcrack_mod_init(void) {
    int ret = 0;
    dev_t devno = 0;
    /* alloc_chrdev_region() dynamically allocates the first free major device number.
    see http://www.makelinux.net/ldd3 chapter 3.2.2. gpio_dev */

    ret = alloc_chrdev_region(&devno, 0, 1, DEVICE_NAME);
    if(ret < 0) {
        printk("%s: Failed to register char device\n", DEVICE_NAME);
        return ret;
    }

    /* start the workqueue here, documentation see: https://lwn.net/Articles/403891/*/
    //my_workqueue = create_singlethread_workqueue("my_queue"); // deprecated since 2.6....
    my_workqueue = alloc_workqueue("my_queue", WQ_HIGHPRI, WQ_CPU_INTENSIVE, 1);
    if(NULL == my_workqueue)
    {
        printk("alloc_workqueue: error allocating queue, %s", DEVICE_NAME);
        return -1;
    }

    /* linked lsit for the results */
    INIT_LIST_HEAD(&(cracked_list_head.list));

    crc32hashcrack_setup(&crc32hashcrack_dev);
    crc32hashcrack_dev.devno = devno;

    ret = cdev_add(&crc32hashcrack_dev.cdev, devno, 1);
    if (ret != 0) {
        printk("cdev_add(): Error adding device %s", DEVICE_NAME);
        return -1;
    }

    /* set to not in use */
    crc32hashcrack_dev.in_use = false;

    /*device is ready for calls to fops functions now!*/
    printk(KERN_ALERT "After loading kernel module, creade /dev entry with 'mknod /dev/%s c %i 0'\n",DEVICE_NAME, MAJOR(devno));
    printk(KERN_ALERT "Remove the module and device with 'rmmod %s' and 'rm /dev/%s'. \n",DEVICE_NAME, DEVICE_NAME);

    return ret;
}


/* Free any dynamically allocated data here.*/
static void __exit crc32hashcrack_mod_exit(void)
{
    /* de-register device to make inaccessible before cleanin up dynmic content -> prevent race condition */
    cdev_del(&crc32hashcrack_dev.cdev);
    unregister_chrdev_region(crc32hashcrack_dev.devno, 1);

    /* cancel waiting/executing workers? */
    flush_workqueue( my_workqueue );
    destroy_workqueue( my_workqueue );

    //clean up linked list like: http://www.roman10.net/2011/07/28/linux-kernel-programminglinked-list/
    struct cracked_results_list *item, *tmp;
    list_for_each_entry_safe(item, tmp, &cracked_list_head.list, list)
    {
        printk("freeing node with input: %d\n", item->input);
        list_del(&item->list);
        kfree(item);
    }
}


MODULE_LICENSE("GPL");
MODULE_AUTHOR("<Marius Pfeffer>");
MODULE_DESCRIPTION("<CRC32 cracking module>");

module_init(crc32hashcrack_mod_init);
module_exit(crc32hashcrack_mod_exit);


